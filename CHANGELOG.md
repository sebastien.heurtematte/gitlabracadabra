# Change Log

## 1.7.0 (2022-05-06)

**Enhancements:**

- Use Version.coerce instead of the strict version

**Tooling:**

- From `renovate`:
  - chore(deps): update dependency types-requests to v2.27.22
  - chore(deps): update dependency mypy to v0.950
  - chore(deps): update dependency python-gitlab to v3.4.0
  - chore(deps): update dependency types-requests to v2.27.24
  - chore(deps): update dependency types-requests to v2.27.25
  - chore(deps): update dependency flake8-debugger to v4.1.2
  - chore(deps): update dependency pylint to v2.13.8
  - chore(deps): update dependency jsonschema to v4.5.1## 1.6.0 (2022-04-26)

## 1.6.0 (2022-04-26)

**Enhancements:**

- urllib3: Lower retry message from WARNING to INFO (!295)

**Tooling:**

- tests: Use temp config instead of mocking (!303)
- From `renovate`:
  - chore(deps): update dependency types-requests to v2.27.19 (!298)
  - chore(deps): update dependency types-html5lib to v1.1.7 (!296)
  - chore(deps): update dependency types-pyyaml to v6.0.6 (!297)
  - chore(deps): update dependency pylint to v2.13.7 (!299)
  - chore(deps): update dependency types-pyyaml to v6.0.7 (!300)
  - chore(deps): update dependency types-requests to v2.27.20 (!301)
  - chore(deps): update dependency flake8-bugbear to v22.4.25 (!302)

## 1.5.0 (2022-04-13)

**Enhancements:**

- retry package connections 3 times (!265)
- Support regexp for Helm package name (!266)
- registry importer: Warn for all HTTP errors (#46, !267)
- python-gitlab v3 compatibility (!240, Debian's [#1009428](https://bugs.debian.org/1009428))

**Tooling:**

- Pin bandit to 1.7.2 (!268)
- Fix A504  prefer the 'msg=' kwarg for assertTrue() diagnostics (!276)
- python-gitlab v3 compatibility (!240)
  - Add TestCase.gitlab_version() to compare pygitlab version
  - python-gitlab v3: config handling has changed again
  - python-gitlab v3: Some urls changed
  - Update application settings schema
  - Always change values when processing a dependency
  - Update Application settings tests for pygitlab v3
- From `renovate`:
  - chore(deps): update dependency types-requests to v2.27.9 (!261)
  - chore(deps): update dependency types-requests to v2.27.10 (!262)
  - chore(deps): update dependency coverage to v6.3.2 (!263)
  - chore(deps): update dependency types-requests to v2.27.11 (!264)
  - Update dependency types-requests to v2.27.12 (!272)
  - Update dependency wemake-python-styleguide to v0.16.1 (!273)
  - Update dependency mypy to v0.940 (!274)
  - Update dependency mypy to v0.941 (!275)
  - Update dependency types-PyYAML to v6.0.5 (!277)
  - Update dependency flake8-bugbear to v22 (!247)
  - Update dependency types-requests to v2.27.13 (!278)
  - Update dependency flake8-assertive to v2.1.0 (!276)
  - Update dependency bandit to v1.7.4 (!279)
  - Update dependency flake8-bandit to v3 (!270, !279)
  - Update dependency flake8-blind-except to v0.2.1 (!280)
  - Update dependency types-requests to v2.27.14 (!281)
  - Update dependency flake8-bugbear to v22.3.20 (!282)
  - Update dependency flake8-bugbear to v22.3.23 (!283)
  - Update dependency mypy to v0.942 (!284)
  - Update dependency pylint to v2.13.0 (!285)
  - Update dependency pylint to v2.13.2 (!286)
  - Update dependency types-requests to v2.27.15 (!287)
  - Update dependency pylint to v2.13.3 (!288)
  - Update dependency pylint to v2.13.4 (!289)
  - Update dependency types-requests to v2.27.16 (!290)
  - Update dependency types-html5lib to v1.1.6 (!291)
  - Update dependency pylint to v2.13.5 (!292)
  - chore(deps): update dependency python-gitlab to v3 (!240)

## 1.4.0 (2022-02-08)

**Enhancements:**

- pypi: Always sync yanked packages when using equal specifier (!229)
- Fix Python GitLab link in doc (!215, #44)
- Update gitlabracadabra.yml with latest parameters (!222)
- retry registry connections 3 times (!257)

**Tooling:**

- Fix WPS324 Found inconsistent `return` statement (!233)
- Fix isort (!213)
- renovate: Remove hourly rate limit for PR creation (!234)
- From `lintian-brush`:
  - Bump debhelper from old 12 to 13 (!208)
  - debian/rules: Drop --fail-missing argument to dh_missing, which is now the default (!208)
  - Update standards version to 4.6.0, no changes needed (!208)
  - Remove constraints unnecessary since buster (!256)
- From `renovate`:
  - Update dependency coverage to v6.1.2 (!209)
  - Update dependency types-PyYAML to v6.0.1 (!210)
  - Update dependency PyYAML to v5.4.1 (!211)
  - Update dependency packaging to v21.3 (!212)
  - Update dependency wemake-python-styleguide to v0.15.3 (!213)
  - Update dependency PyYAML to v6 (!214)
  - Pin flake8-broken-line==0.3.0 (!213)
  - Pin flake8 (!213)
  - Pin pycodestyle (!213)
  - Pin pep8-naming (!213)
  - Update dependency types-requests to v2.26.1 (!216)
  - Update dependency coverage to v6.2 (!217)
  - Update dependency flake8-bugbear to v21.11.29 (!219)
  - Update dependency flake8-rst-docstrings to v0.2.5 (!220)
  - Update dependency types-html5lib to v1.1.3 (!223)
  - Update dependency types-requests to v2.26.2 (!224)
  - Update dependency jsonschema to v4.3.2 (!225)
  - Update dependency mypy to v0.930 (!226)
  - Update dependency jsonschema to v4.3.3 (!227)
  - Update dependency types-html5lib to v1.1.4 (!228)
  - chore(deps): update dependency pylint to v2.12.2 (!231)
  - chore(deps): update dependency types-requests to v2.27.0 (!232)
  - chore(deps): update dependency wemake-python-styleguide to v0.16.0 (!233)
  - chore(deps): update dependency flake8-broken-line to v0.4.0 (!218)
  - chore(deps): update dependency pep8-naming to v0.12.1 (!221)
  - chore(deps): update dependency types-html5lib to v1.1.5 (!235)
  - chore(deps): update dependency types-requests to v2.27.1 (!237)
  - chore(deps): update dependency flake8-assertive to v2 (!239)
  - chore(deps): update dependency types-pyyaml to v6.0.2 (!236)
  - chore(deps): update dependency flake8 to v4 (!238)
  - chore(deps): update dependency pycodestyle to v2.8.0 (!238 superseding !230)
  - chore(deps): update dependency types-requests to v2.27.5 (!243)
  - chore(deps): update dependency mypy to v0.931 (!244)
  - chore(deps): update dependency types-pyyaml to v6.0.3 (!242)
  - chore(deps): update dependency flake8-comprehensions to v3.8.0 (!245)
  - chore(deps): update dependency flake8-tidy-imports to v4.6.0 (!246)
  - chore(deps): update dependency types-requests to v2.27.6 (!248)
  - chore(deps): update dependency jsonschema to v4.4.0 (!249)
  - chore(deps): update dependency types-requests to v2.27.7 (!250)
  - chore(deps): update dependency coverage to v6.3 (!251)
  - chore(deps): update dependency types-pyyaml to v6.0.4 (!253)
  - chore(deps): update dependency types-requests to v2.27.8 (!254)
  - chore(deps): update dependency coverage to v6.3.1 (!255)
  - chore(deps): update dependency semantic_version to v2.9.0 (!258)

## 1.3.0 (2021-11-10)

**New features:**

- Mirror PyPI packages (!183, !184, !185, !192, !193)
- Add squash_option support (!189, !198)
- Add user state support (!197, #41)

**Fixes:**

- package mirrors:
  - Keep original request URL, to avoid too long filename (!186)
  - Consider 202 Accepted as upload success (!190)
  - Use default Accept-Encoding (!196)
- image mirrors:
  - Force syncing all platforms when digest is set (!194)
  - Do not register blobs from manifest lists (!195)
  - Catch manifest import errors (!200, #42)

**Tooling:**

- Do not build Universal Wheel anymore (!182)
- Fix pep8 since wemake update (!199)
- Add renovate config (!201)
  - Pin dependencies (!202)
  - Update dependency isort to v5 (!203)
  - Pin all python packages (!204)
  - Pin mypy dependencies (!204)
  - Pin CI images (!205)
- Build deb on bullseye (!197)

## 1.2.0 (2021-06-20)

**New features:**

- Helm chart repository support (!169, !170, !171, #38, #39, !174, !179)
  - With repo_url, package_name, versions, semver, limit and channel parameters

**Enhancements:**

- warn instead of info for not found and other non-ok status (!172)
- CI:
  - Fix mock_calls on buster (!173)
  - Fix ProxyError message on buster (!173)
  - Fix buster linting issue (!179)
  - Update to latest mypy and types-* packages (!176)
  - Fix mypy errors (!177)
  - PyGitlab now converts arrays to comma-separated list as string (!177)
  - Reduce noqa usage (!178)

**Fixes:**

- Fix format order in package file upload logging (!172)
- Warn on HTTP errors instead of failing (#39, !172)
- Fix github assets upload (!175)
- Ensure parameter is mangled on change. Fixes: first_day_of_week in
  application settings (!177)

## 1.1.0 (2021-05-21)

**New features:**

- :rocket: Package mirroring (#31):
  - Raw URLs to generic packages (!156)
  - Github releases to generic packages
    (#34, #35, #37, !159, !160, !161, !162, !163, !164, !165)

**Enhancements:**

- Improve JSONschema errors
- Image mirrors features:
  - Store Blob sizes to avoid uneeded HEAD request when printing stats (!153)
  - Better manifest v1 handling: Avoid 404 errors when printing stat and with
    mounted blobs (#33, !153, !154, !155)
- Misc:
  - Fix hadolint: Multiple consecutive `RUN` instructions (!153)
  - Update requirements for libgit2 1.0 transition (!156)

## 1.0.0 (2021-04-17)

**Important fixes:**

- **Security**: Ensure TLS certificates is verified with container registries (!139)

**Enhancements:**

- Image mirrors features:
  - Implement SemVer (!148)
  - Support manifest v1 (#28, !143, !145)
  - Add OCI Image Media Types (!149)
  - Allow to synchronize image by digest (!138)
  - Handle registries without Docker-Content-Digest header (!141)
  - Avoid checking signed manifest digest (#29, !146)
  - Increase the chunk size to 50MB (!142)
  - Improve heuristics:
    - Always GET manifests as they are small (!146)
    - Register seen blobs from manifests (!146)
    - Check if blob exists before uploading (!146)
    - Avoid unneeded HTTP query just to get size (!146)
  - Allow tag regexp for string-form from too (!147)
- Lower NOT Deleting %s (not found) from INFO to DEBUG (!144)

**Fixes:**

- Image mirrors fixes:
  - Avoid infinite stack trace with cache_path (!140)
  - Fix blob mount (#30, !146)
  - Handle token expiration (!150)
- Use hadolint/hadolint:latest-alpine for hadolint job (!138)

## 0.9.1 (2021-03-20)

**Enhancements:**

- Sync debian/source/options tar-ignore with .gitignore (!134)
- Add dep5 tests (autopkgtest) (!135)

## 0.9.0 (2021-03-19)

**New features:**

- :rocket: Docker image mirroring (#13):
  - Basic mirroring (!114, !117, !118, !119 , !120, !123)
  - Support selecting images by base+repositories+tags (!128)
  - Tag matching with regexps (!129)
  - Bug fixes (#27, !127)
- Support for multiple GitLab connections, including `mirrors` (!130, !131)

**Enhancements:**

- Use shorter default logging format (!125)
- Improve error when `extends:` is not found (!101)
- mirrors: Enable automatic proxy detection when available (#25, !115, !125)
- User: Add skip_reconfirmation, note (!116)
- Docker image:
  - Use Debian package on Docker image (!102)
    - Removes pip usage
    - Ensure certificate usage is centralized (no `/usr/local/lib/python3.7/dist-packages/certifi/cacert.pem`)
    - Use `bullseye`, as `buster` packages are older
  - Test the Docker image (!103)
- Documentation:
  - Add the missing `v` prefix in Docker image tag (!99)
  - Fix docker command, and mention other images (!105)
  - Document ability to mirror Git repositories (!108)
- Code cleanup and developpement tooling:
  - Improve pep8 (!98, !109, !110), !111
  - Use "build" job from AutoDevOps (!104)
  - Refactor test utils (!106)
  - Drop python2 support (!110)
  - Initial mypy checks (!114)

**Fixes:**

- Always remove trailing slash, even when type is specified (#23, !100)
- Fix: is_admin -> admin param for creation or update (#26, !112, !122)
- Create cache dir (#19, !121, !124)

## 0.8.0 (2021-02-09)

Thanks to Sébastien Heurtematte who contributed two merge requests (!91, !93)!

**New features:**

- Mirror branch mapping (!83)
- Project and Group Boards (!85, !86)
- Add support for Docker retention policy (!84)
- Add rename_branches parameter (!89)

**Enhancements:**

- Various pep8 improvements and added flake8 plugins (!87, !88)
- Enhance docker build and debug (!93)
- Improve CI (!95)
- Documentation:
  - Document "Using gitlabracadabra in GitLab CI" (!89)
  - doc: Add missing usage example in "Using packages" (!89)

**Fixes:**

- Use api param namespace_id to parent_id for subgroup creation (!91)

## 0.7.0 (2020-10-29)

**New features:**

- Implement group of groups (!74)
- Implement pipeline schedules (including variables) (!75)
- Add aggregate merge strategy (!77)

**Enhancements:**

- Document required release dependencies (!72)
- Update copyright to 2020 (!73)
- Fix Unknown merge strategy error message (!76)
- Consider null label description as same as empty (!78)
- Ship all documentation in deb package (!79)

## 0.6.0 (2020-06-18)

**Important fixes:**

- **Security**: Mirroring: Only send Private-Token on gitlab remote (!63)

**New features:**

- Implement project's webhooks (!67)

**Enhancements:**

- Add forking\_access\_level, pages\_access\_level, emails\_disabled and suggestion\_commit\_message
  to projects (!66, !69)
- Add support for project's allow\_merge\_on\_skipped\_pipeline and autoclose\_referenced\_issues (!70)
- tests: Ignore host and port on match (!67)
- Documentation:
  - Reorder project and group parameters as in WebUI (!65)
  - Update release doc (!62)
  - Sync README with requirements.txt (pygit2 is now needed) (!69)

**Fixes:**

- Pin pygit2 to 1.0.3 to fix FTBFS (!64)
- CI and tests fixes (!68):
  - Fix "jobs:dast config key may not be used with `rules`: except"
  - Fix "E712 comparison to False should be 'if cond is False:' or 'if not cond:'"
  - Fix "E302 expected 2 blank lines, found 1"

## 0.5.0 (2020-01-23)

**Backwards incompatible changes:**

- `extends` now use `deep` merging by default, to restore the previous behavior
  use `replace` merge strategy

**New features:**

- `extends` now supports two merge strategies: `deep` (the default) and `replace` (!54 and !56)
- `extends` now supports multiple parents (!56)
- Repository mirroring (!52, !60)
- Add support for project and group milestones (!58)

**Enhancements:**

- Add remove\_source\_branch\_after\_merge parameter to projects (!46)
- Add default\_ci\_config\_path parameter to application settings (!47)
- Add emails\_disabled, project\_creation\_level, subgroup\_creation\_level, require\_two\_factor\_authentication,
  two\_factor\_grace\_period, auto\_devops\_enabled to groups (!59)
- Sync with current project and group parameters (!59)
- Documentation:
  - Add Table of Contents (!48)
  - Generate markdown doc from source (!48)
  - Document releasing a new version (!49)
  - Various documentation enhancements (!51)
  - Add general action file documentation (!53)
- Tests:
  - Only install packages for the gitlabracadabra.tests.unit package (!55)

**Fixes:**

- User:
  - Fix typo of user param: `s/admin/is_admin/` (!57)
  - Remove username as param, as this is the id (!57)
  - Fix `CREATE_KEY` handling (!57)
  - user's skip_confirmation is a create param (!57)
  - Do not try to change create-only params for users (!57)
- Tests:
  - Clean up before testing group member not found (!50)

## 0.4.0 (2019-11-14)

**New features:**

- Add delete\_object param (!43)
- Add support for include (#7, !42)
- Add project and group labels support (#2, !36, !39, !40)
- Add support for application settings (#4, !34)
- Package uploaded to Debian (!45)

**Enhancements:**

- Add support for latest project params (!33)
- Test YAML file loading (!42)
- Ensure that content is not changed during processing (!41)
- User password is only set on user creation (!41)
- Assert all cassettes are "all played" (!37, !38)

**Fixes:**

- Don't fail when some variables params are not available (!44)
- Python-gitlab 1.13 still has incorrect labels support (!45)

## 0.2.1 (2019-06-20)

**New features:**

- Debian packages (!24, !25)
- Project and group variables (!29)
- Project archived parameter (!27)

**Enhancements:**

- Catch members deletion errors (!30)
- Skip changing parameter only when not available (i.e. handle null as current value) (!28)
- Catch REST errors of \_process\_ methods (!26)
- Enforce draft 4 of jsonschema (!25)
- Skip protected tags tests on python-gitlab < 1.7.0 (!24)
- Workaround "all=True" in API calls with python-gitlab < 1.8.0 (!24)
- Move VCR fixtures closer to the tests (!23)
- Centralize VCR configuration (!23)

## 0.1.0 (2019-05-25)

**New features:**

- Projects
  - Basic parameters
  - Project members
  - Add protected branches support
  - Protected tags
  - Add latest project parameters
    (mirror\_user\_id, only\_mirror\_protected\_branches, mirror\_overwrites\_diverged\_branches, packages\_enabled)
  - Add initialize\_with\_readme support to projects
- Groups:
  - Basic parameters
  - Group members
  - Add latest group parameters
    (membership\_lock, share\_with\_group\_lock, file\_template\_project\_id, extra\_shared\_runners\_minutes\_limit)
- Users:
  - Basic parameters
