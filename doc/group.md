# Table of Contents <!-- omit in toc -->

- [Group lifecycle](#group-lifecycle)
- [General Settings](#general-settings)
  - [Naming, visibility](#naming-visibility)
  - [Permissions, LFS, 2FA](#permissions-lfs-2fa)
- [CI / CD Settings](#ci-cd-settings)
  - [Variables](#variables)
  - [Auto DevOps](#auto-devops)
- [Members](#members)
- [Issues](#issues)
  - [Boards](#boards)
  - [Labels](#labels)
  - [Milestones](#milestones)
- [Undocumented](#undocumented)

# Group lifecycle

`gitlab_id` GitLab id:
```yaml
mygroup/:
  gitlab_id: gitlab
```

More information can be found [here](action_file.md#gitlab_id)

`create_object` Create object if it does not exists:
```yaml
mygroup/:
  create_object: true # or false
```

`delete_object` Delete object if it exists:
```yaml
mygroup/:
  delete_object: true # or false
```

# General Settings

## Naming, visibility

`name` The name of the group:
```yaml
mygroup/:
  name: My name
```

`description` The group’s description:
```yaml
mygroup/:
  description: My description
```

`visibility` The group’s visibility. Can be private, internal, or public.:
```yaml
mygroup/:
  visibility: private # one of private, internal, public
```

## Permissions, LFS, 2FA

`request_access_enabled` Allow users to request member access.:
```yaml
mygroup/:
  request_access_enabled: true # or false
```

`share_with_group_lock` Prevent sharing a project with another group within this group:
```yaml
mygroup/:
  share_with_group_lock: true # or false
```

`emails_disabled` Disable email notifications:
```yaml
mygroup/:
  emails_disabled: true # or false
```

`lfs_enabled` Enable/disable Large File Storage (LFS) for the projects in this group:
```yaml
mygroup/:
  lfs_enabled: true # or false
```

`project_creation_level` Determine if developers can create projects in the group:
```yaml
mygroup/:
  project_creation_level: noone # one of noone, maintainer, developer
```

`subgroup_creation_level` Allowed to create subgroups:
```yaml
mygroup/:
  subgroup_creation_level: owner # one of owner, maintainer
```

`require_two_factor_authentication` Require all users in this group to setup Two-factor authentication:
```yaml
mygroup/:
  require_two_factor_authentication: true # or false
```

`two_factor_grace_period` Time before Two-factor authentication is enforced (in hours):
```yaml
mygroup/:
  two_factor_grace_period: 42
```

`membership_lock` Prevent adding new members to project membership within this group:
```yaml
mygroup/:
  membership_lock: true # or false
```

# CI / CD Settings

## Variables

`variables` The list of group's variables:
```yaml
mygroup/:
  variables:
    - key: DAST_DISABLED
      value: '1'
      masked: false
      protected: false
      variable_type: env_var

```

`unknown_variables` What to do with unknown variables (`warn` by default).:
```yaml
mygroup/:
  unknown_variables: warn # one of warn, delete, remove, ignore, skip
```

## Auto DevOps

`auto_devops_enabled` Default to Auto DevOps pipeline for all projects within this group:
```yaml
mygroup/:
  auto_devops_enabled: true # or false
```

# Members

`members` Members:
```yaml
mygroup/:
  members:
    foo: developer
    bar: maintainer # one of guest, reporter, developer, maintainer, owner

```

`unknown_members` What to do with unknown members (`warn` by default).:
```yaml
mygroup/:
  unknown_members: warn # one of warn, delete, remove, ignore, skip
```

`groups` Groups:
```yaml
mygroup/:
  groups:
    group/foo: guest
    group/bar: reporter # one of guest, reporter, developer, maintainer

```

`unknown_groups` What to do with unknown groups (`warn` by default).:
```yaml
mygroup/:
  unknown_groups: warn # one of warn, delete, remove, ignore, skip
```

# Issues

## Boards

`boards` The list of group's boards:
```yaml
mygroup/:
  boards:
    - name: My group board
      # old_name: Development # Use this to rename a board
      hide_backlog_list: false
      hide_closed_list: false
      lists:
        - label: TODO
        - label: WIP

```

`unknown_boards` What to do with unknown boards (`warn` by default).:
```yaml
mygroup/:
  unknown_boards: warn # one of warn, delete, remove, ignore, skip
```

`unknown_board_lists` What to do with unknown board lists (`delete` by default).:
```yaml
mygroup/:
  unknown_board_lists: warn # one of warn, delete, remove, ignore, skip
```

## Labels

`labels` The list of group's labels:
```yaml
mygroup/:
  labels:
    - name: bug
      color: '#d9534f'
      description: ''
    - name: confirmed
      color: '#d9534f'
      description: ''
    - name: critical
      color: '#d9534f'
      description: ''
    - name: discussion
      color: '#428bca'
      description: ''
    - name: documentation
      color: '#f0ad4e'
      description: ''
    - name: enhancement
      color: '#5cb85c'
      description: ''
    - name: suggestion
      color: '#428bca'
      description: ''
    - name: support
      color: '#f0ad4e'
      description: ''

```

`unknown_labels` What to do with unknown labels (`warn` by default).:
```yaml
mygroup/:
  unknown_labels: warn # one of warn, delete, remove, ignore, skip
```

## Milestones

`milestones` The list of group's milestones:
```yaml
mygroup/:
  milestones:
    - title: '1.0'
      description: Version 1.0
      due_date: '2021-01-23' # Quotes are mandatory
      start_date: '2020-01-23' # Quotes are mandatory
      state: active # or closed

```

`unknown_milestones` What to do with unknown milestones (`warn` by default).:
```yaml
mygroup/:
  unknown_milestones: warn # one of warn, delete, remove, ignore, skip
```

# Undocumented

`extra_shared_runners_minutes_limit` (admin-only) Extra pipeline minutes quota for this group.:
```yaml
mygroup/:
  extra_shared_runners_minutes_limit: 42
```

`file_template_project_id` (Premium) The ID of a project to load custom file templates from:
```yaml
mygroup/:
  file_template_project_id: 42
```

`shared_runners_minutes_limit` (admin-only) Pipeline minutes quota for this group.:
```yaml
mygroup/:
  shared_runners_minutes_limit: 42
```


