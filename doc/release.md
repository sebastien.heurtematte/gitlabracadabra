# Releasing GitLabracadabra

Ensure required dependencies are installed:

```shell
sudo apt-get install -y devscripts dh-python git-buildpackage
```

Define the previous, target and next version:

```shell
previous=1.4.0
target=1.5.0
next=1.6.0
```

Edit the release notes:

```shell
git log --oneline --reverse --no-decorate v$previous..HEAD
editor CHANGELOG.md
```

Update `debian/changelog`, `gitlabracadabra/__init__.py` and `README.md`:

```shell
dch -v $target -D unstable 'New release'
sed -i "s/__version__ = .*/__version__ = '$target'/" gitlabracadabra/__init__.py
sed -i "s/$previous/$target/g" README.md
```

Test the build and re-generate the doc:

```shell
tox

python3 -m pip install --user -r requirements.txt -r test-requirements.txt --upgrade
python3 setup.py build
python3 setup.py install --user
python3 setup.py test

for t in project group user application_settings; do
  ~/.local/bin/gitlabracadabra --doc-markdown $t > doc/$t.md
done
```

Commit and create the release merge request:

```shell
git commit -am"Release $target"
git push -o merge_request.create \
         -o merge_request.merge_when_pipeline_succeeds \
         origin HEAD:mr$target
```

Build the pip package and upload to test.pypi.org:

```shell
python3 -m pip install --user --upgrade setuptools wheel
python3 setup.py sdist bdist_wheel

python3 -m pip install --user --upgrade twine
python3 -m twine upload --repository-url https://test.pypi.org/legacy/ "dist/gitlabracadabra-$target"*
```

Once the MR is merged, tag the release:

```shell
git pull --rebase --prune
git tag "v$target"
git push origin "v$target"
```

And upload artifacts to PyPI and Debian:

```shell
python3 -m twine upload "dist/gitlabracadabra-$target"*

gbp buildpackage -S -d
dput "../gitlabracadabra_${target}_source.changes"
```

## After the release

Development mode for the next version:

```shell
dch -v "${next}~a0" -D UNRELEASED 'New release'
sed -i "s/__version__ = .*/__version__ = '${next}a0'/" gitlabracadabra/__init__.py
```

Commit and create merge request:

```shell
git commit -am"Development mode for $next"
git push -o merge_request.create \
         -o merge_request.merge_when_pipeline_succeeds \
         origin HEAD:dev$next
```
